(function(ng, log) {
	ng.module('table-demo', []);
	ng.module('table-demo').controller('DemoCtrl', DemoCtrl);
	DemoCtrl.$inject  = ['dataservices', '$filter'];;
	function DemoCtrl(dataservices, $filter) {
		var data = [];
		/* jshint validthis: true */
		var ctrl = this;
		ctrl.orderBy = null;
		ctrl.reverse = false;
		ctrl.switchOrder = switchOrder;
		dataservices.getUsers().then(function(res) {
			data = res.data;
			updateData();
		});
		function sortBy(item) {
			if(ctrl.orderBy !== null) {
				var i = 0;
				for(var propertyName in item) {
					if(i === ctrl.orderBy) {
						return item[propertyName];
					}
					i++;
				}
			} else return item;
		}
		function switchOrder(evt) {
			evt.preventDefault;
			var num = evt.currentTarget.parentNode.cellIndex;
			if(num === ctrl.orderBy) {
				ctrl.orderBy = ctrl.reverse?null:num;
				ctrl.reverse = !ctrl.reverse;
			} else {
				ctrl.orderBy = num;
				ctrl.reverse = false;
			}
			updateData();
		}
		function updateData() {
			ctrl.users = $filter('orderBy')(data, sortBy, ctrl.reverse);
		}
	}
})(angular, console.log);
(function(ng, log) {
	ng.module('table-demo').factory('dataservices', services);
	services.$inject  = ['$http'];
	function services($http) {
		return {
			getUsers : getUsers
		};
		function getUsers() {
			return $http.get('users.json');
		}
	}
})(angular, console.log);
